FROM mcr.microsoft.com/dotnet/core/aspnet:3.0-alpine AS base
WORKDIR /app
EXPOSE 80
EXPOSE 443

RUN apk add --update nodejs npm && npm -v && node -v

FROM mcr.microsoft.com/dotnet/core/sdk:3.0-alpine AS build
WORKDIR /src
COPY ["ECommerce.csproj", ""]
RUN dotnet restore "./ECommerce.csproj"
COPY . .
WORKDIR "/src/."
RUN dotnet build "ECommerce.csproj" -c Release -o /app/build

FROM build AS publish
RUN dotnet publish "ECommerce.csproj" -c Release -o /app/publish

FROM base AS final
WORKDIR /app
COPY --from=publish /app/publish .
ENTRYPOINT ["dotnet", "ECommerce.dll"]